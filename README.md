# Računalna grafika

Svaki labos ima makefile kojim se prevodi izvorni kod. Potrebno je smjestiti se u direktorij labosa i pokrenuti naredbu `make` u naredbenom retku. Rezultat prevođenja je `run` izvršna datoteka. Svaka izvršna datoteka ispisuje `Usage` prilikom pokretanja bez adekvatnih argumenata, pa ostavljam korisniku da otkrije kako se svaki labos pokreće.

## lab2

Nespretnim baratanjem git-om sam uspio nepovratno pobrisati sve kodove za drugi labos, no izvršna datoteka je spašena zahvaljujući .gitignore-u. Stoga, u nedostatku kodova, priložena je izvršna datoteka kompajlirana gcc-om na x86 Linux-u kao dokaz da je labos postojao.
