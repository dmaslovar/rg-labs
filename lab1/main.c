#include <stdio.h>
#include <GL/glut.h>
#include <math.h>
#include "vectors.c"

#define INCREMENT 0.01
#define ORDER 4

struct matrix *zAxis;

int nOfPoints = 0;
struct matrix *controlPoints;

struct matrix *bSplineMatrix, *bTangMatrix;
double bSplineM[16] = {-1, 3, -3, 1,
                        3, -6, 3, 0,
                        -3, 0, 3, 0,
                        1, 4, 1, 0};
double bTangM[12] = {-1, 3, -3, 1,
					2, -4, 2, 0,
					-1, 0, 1, 0};

int animSegment = 0;
double animT = 0.0;

int objVN = 0;
int objFN = 0;
struct matrix **objV;
int **objF;

void err(int code, char *message) {
	printf("error: %s\n", message);
	exit(code);
}

struct matrix* getBTang(int segment, double t) {
	if (segment < 0 || segment > nOfPoints - ORDER) err(4, "bad segment");
	if (t < 0.0 || t > 1.0) err(5, "bad t");

	struct matrix *tMatrix = m_new(1, ORDER - 1), *mult;
	for (int i = 0; i < ORDER - 1; i++) {
		tMatrix->m[i] = pow(t, ORDER - 1 - i) / 2.0;
	}
	
	mult = m_mult(tMatrix, bTangMatrix);
	free_matrix(tMatrix);
	tMatrix = m_new(1, controlPoints->y);

	for (int i = 0; i < controlPoints->y; i++) {
		tMatrix->m[i] = 0;
		for (int j = 0; j < ORDER; j++) {
			tMatrix->m[i] += mult->m[j] * controlPoints->m[(j + segment) * controlPoints->y + i];
		}
	}
	free_matrix(mult);
	
	return tMatrix;
}

struct matrix* getBSpline(int segment, double t) {
	if (segment < 0 || segment > nOfPoints - ORDER) err(4, "bad segment");
	if (t < 0.0 || t > 1.0) err(5, "bad t");

	struct matrix *tMatrix = m_new(1, ORDER), *mult;
	for (int i = 0; i < ORDER; i++) {
		tMatrix->m[i] = pow(t, ORDER - 1 - i) / 6.0;
	}
	
	mult = m_mult(tMatrix, bSplineMatrix);
	free_matrix(tMatrix);
	tMatrix = m_new(1, controlPoints->y);

	for (int i = 0; i < controlPoints->y; i++) {
		tMatrix->m[i] = 0;
		for (int j = 0; j < ORDER; j++) {
			tMatrix->m[i] += mult->m[j] * controlPoints->m[(j + segment) * controlPoints->y + i];
		}
	}
	free_matrix(mult);

	return tMatrix;
}
/*
void drawCube() {
	glBegin(GL_LINE_LOOP);
	glVertex3d(-1, -1, -1);
	glVertex3d(-1, -1, 1);
	glVertex3d(-1, 1, 1);
	glVertex3d(-1, 1, -1);
	glEnd();
	
	glBegin(GL_LINE_LOOP);
	glVertex3d(1, -1, -1);
	glVertex3d(1, -1, 1);
	glVertex3d(1, 1, 1);
	glVertex3d(1, 1, -1);
	glEnd();
	
	glBegin(GL_LINES);
	glVertex3d(-1, -1, -1);
	glVertex3d(1, -1, -1);
	
	glVertex3d(-1, -1, 1);
	glVertex3d(1, -1, 1);
	
	glVertex3d(-1, 1, -1);
	glVertex3d(1, 1, -1);
	
	glVertex3d(-1, 1, 1);
	glVertex3d(1, 1, 1);
	glEnd();
}
*/
void drawObject() {
	int *face;
	for (int i = 0; i < objFN; i++) {
		face = objF[i];
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 3; j++) {
			glVertex3dv(objV[face[j] - 1]->m);
		}
		glEnd();
	}
}

void drawBSpline() {
	struct matrix *point;
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i <= nOfPoints - ORDER; i++) {
		for (double t = 0; t <= 1.0; t += INCREMENT) {
			point = getBSpline(i, t);
			glVertex3dv(point->m);
			free_matrix(point);
		}
	}
	glEnd();
}

void renderScene() {
	struct matrix *tan, *pos, *axis;
	double phi;

	glColor3d(1.0, 0, 0.3);
	glPushMatrix();
	glScaled(5.0, 5.0, 1.0);
	drawBSpline();
	glPopMatrix;
	
	pos = getBSpline(animSegment, animT);
	tan = getBTang(animSegment, animT);
	axis = m_vector_mult(zAxis, tan);
	phi = acos(m_scalar_mult(zAxis, tan) / m_vector_norm(zAxis) / m_vector_norm(tan)) * 180.0 / M_PI;
	
	glColor3d(1.0, 1.0, 1.0);
	glPushMatrix();
	glTranslated(pos->m[0], pos->m[1], pos->m[2]);
	glRotated(phi, axis->m[0], axis->m[1], axis->m[2]);
	glScaled(10, 10, 10);
	drawObject();
	glPopMatrix();
	
	free_matrix(pos);
	free_matrix(tan);
	free_matrix(axis);
}

void display() {
	glClearColor(0, 0, 0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(25.0, 200.0, 25.0, 25.0, 0.0, 25.0, 0.0, 0.0, 1.0);
	renderScene();
	glutSwapBuffers();
}

void reshape(int width, int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-0.6, 0.6, -0.6, 0.6, 1.5, 250.0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, (GLsizei) width, (GLsizei) height);
}

void idle() {
    animT += INCREMENT;
    if (animT > 1.0) {
        animT = 0.0;
        animSegment = (animSegment + 1) % (nOfPoints - ORDER + 1);
    }
	glutPostRedisplay();
}

void loadControlPoints(char *fileName) {
	FILE* f = fopen(fileName, "r");
	if (f == NULL) err(1, "cannot open file");
	
	char buf[64];
	while (fscanf(f, "%[^\n]\n", buf) != EOF) {
		nOfPoints++;
	}
	fclose(f);
	
	int rows = nOfPoints, cols = 3;
	
	controlPoints = m_new(rows, cols);
	f = fopen(fileName, "r");
	if (f == NULL) err(1, "cannot open file");
	
	for (int i = 0; i < nOfPoints; i++) {
		for (int j = 0; j < 3; j++) {
			if (fscanf(f, "%lf", controlPoints->m + i*cols + j) == 0)
				err(2, "cannot read points");
		}
	}
	fclose(f);
}

void loadObject(char *fileName) {
	FILE* f = fopen(fileName, "r");
	if (f == NULL) err(1, "cannot open file");
	
	char buf[64];
	memset(buf, 0, sizeof(char) * 64);
	while (fscanf(f, "%[^\n]\n", buf) != EOF) {
		if (buf[0] == 'v') objVN++;
		else if (buf[0] == 'f') objFN++;
		memset(buf, 0, sizeof(char) * 64);
	}
	fclose(f);
	
	objV = (struct matrix**) malloc(sizeof(struct matrix*) * objVN);
	objF = (int**) malloc(sizeof(int*) * objFN);
	
	f = fopen(fileName, "r");
	if (f == NULL) err(1, "cannot open file");
	
	int vIndex = 0, fIndex = 0;
	struct matrix *tmpM;
	int *tmpA;
	
	memset(buf, 0, sizeof(char) * 64);
	while (fscanf(f, "%[^\n]\n", buf) != EOF) {
		if (buf[0] == 'v') {
			tmpM = m_new(1, 3);
			sscanf(buf, "v %lf %lf %lf", tmpM->m, tmpM->m+1, tmpM->m+2);
			objV[vIndex] = tmpM;
			vIndex++;
		} else if (buf[0] == 'f') {
			tmpA = (int*) malloc(sizeof(int) * 3);
			sscanf(buf, "f %d %d %d", tmpA, tmpA+1, tmpA+2);
			objF[fIndex] = tmpA;
			fIndex++;
		}
		memset(buf, 0, sizeof(char) * 64);
	}
	fclose(f);
}

int main(int argc, char **argv) {
	if (argc < 3)
		err(3, "Usage: main ctrl_points_file obj_file");
	
	zAxis = m_new(1, 3);
	zAxis->m[2] = 1.0;

    bSplineMatrix = (struct matrix*) malloc(sizeof(struct matrix));
    bSplineMatrix->x = 4;
    bSplineMatrix->y = 4;
    bSplineMatrix->m = bSplineM;
    
    bTangMatrix = (struct matrix*) malloc(sizeof(struct matrix));
    bTangMatrix->x = 3;
    bTangMatrix->y = 4;
    bTangMatrix->m = bTangM;

	loadControlPoints(argv[1]);
	loadObject(argv[2]);
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(500, 500);
	glutCreateWindow("1. Laboratorijska vježba");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(idle);
	glutMainLoop();
}

