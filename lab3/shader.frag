#version 150 core

in vec3 Position;
in vec3 Color;
in vec3 Normal;
out vec4 outColor;

uniform vec3 light_pos;
uniform vec3 light_col;
uniform vec3 eye;

void main() {
	vec3 ambient = 0.05 * Color;

	vec3 light_dir = normalize(light_pos - Position);
	vec3 normal = normalize(Normal);
	float diff = max(dot(light_dir, normal), 0.0);
	vec3 diffuse = diff * Color;

	vec3 view_dir = normalize(eye - Position);
	vec3 reflected = reflect(-light_dir, normal);
	float spec = pow(max(dot(eye, reflected), 0.0), 8.0);
	vec3 specular = vec3(0.3) * spec;
	
//	outColor = vec4(Color, 1.0);
	outColor = vec4(ambient + diffuse + specular, 1.0);
}
