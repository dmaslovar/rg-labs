#version 150 core

in vec3 position;
in vec3 color;
out vec3 Position;
out vec3 Color;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main() {
	Color = color;
	gl_Position = proj * view * model * vec4(position, 1.0);
	Position = gl_Position.xyz;
	Normal = position;
//	gl_Position = vec4(position, 1.0);
}
