#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glut.h>
#include <math.h>

#define VERT_SHAD "shader.vert"
#define FRAG_SHAD "shader.frag"

#define OBJ_FILE "porsche.obj"

float elapsed_time = 0.0f;

struct graphObj {
	int v_size;
	float *vertices;
	int e_size;
	GLuint *elements;
	int n_size;
	float *normals;
	GLuint vbo;
	GLuint ebo;
	GLint posAttrib;
	GLint colAttrib;
	GLint normAttrib;
	GLuint vao;
};

GLuint shaderProgram;
GLint uniModel;
struct graphObj *car;

int fileSize(char *filename) {
	struct stat st;

	if (stat(filename, &st) == 0)
		return st.st_size;

	fprintf(stderr, "Cannot determine size of %s: %s\n", filename, strerror(errno));
	return -1;
}

int loadFile(char *filename, char **content) {
	int fd, len;
	char *buffer;

	if ((len = fileSize(filename)) == -1)
		return -1;
	buffer = (char*) malloc(len + 1);
	buffer[len] = 0;
	if ((fd = open(filename, O_RDONLY)) == -1) {
		fprintf(stderr, "Cannot open file %s: %s\n", filename, strerror(errno));
		return -1;
	}
	if (read(fd, buffer, len) == -1) {
		fprintf(stderr, "Cannot load file %s: %s\n", filename, strerror(errno));
		return -1;
	}

	close(fd);
	*content = buffer;

	return len;
}

GLuint compileShader(char *shader, GLenum which) {
	char *source;
	GLuint shaderId;
	GLint status;

	if (loadFile(shader, &source) == -1)
		return -1;

	shaderId = glCreateShader(which);
	glShaderSource(shaderId, 1, (const GLchar**) &source, NULL);
	glCompileShader(shaderId);
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		char buffer[512];
		glGetShaderInfoLog(shaderId, 512, NULL, buffer);
		fprintf(stderr, "Error compiling %s:\n%s\n", shader, buffer);
		return -1;
	}

	return shaderId;
}

GLuint compileShaders(char *vertShad, char *fragShad) {
	GLuint vertexShader, fragmentShader, shaderProgram;

	if ((vertexShader = compileShader(vertShad, GL_VERTEX_SHADER)) == -1)
		return -1;
	if ((fragmentShader = compileShader(fragShad, GL_FRAGMENT_SHADER)) == -1)
		return -1;

	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);

	return shaderProgram;
}

int countLines(char start, char *file) {
	FILE* f;
	int n = 0;

	if ((f = fopen(file, "r")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", file, strerror(errno));
		return -1;
	}

	char buf[128];
	while (fscanf(f, "%[^\n]\n", buf) != EOF) {
		if (buf[0] == start) n++;
	}
	fclose(f);

	return n;
}

int loadVertices(struct graphObj *obj, char *filename) {
	char buf[64];
	int vIndex = 0;
	FILE* f = fopen(filename, "r");
	if (f == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", filename, strerror(errno));
		return -1;
	}

	while (fscanf(f, "%[^\n]\n", buf) != EOF) {
		if (buf[0] == 'v') {
			sscanf(buf, "v %f %f %f", obj->vertices + vIndex, obj->vertices + (vIndex + 1), obj->vertices + (vIndex + 2));
			//colour
			for (int i = 3; i < 6; i++)
				obj->vertices[vIndex + i] = 1.0f;
			vIndex += 6;
		}
	}

	return 0;
}
/*
int loadNormals(float *normals, char *filename) {
	char buf[64];
	int nIndex = 0;
	FILE* f = fopen(filename, "r");
	if (f == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", filename, strerror(errno));
		return -1;
	}

	while (fscanf(f, "%[^\n]\n", buf) != EOF) {
		if (buf[0] == 'v') {
			sscanf(buf, "v %f %f %f", normals + nIndex, normals + (nIndex + 1), normals + (nIndex + 2));
			nIndex += 3;
		}
	}

	return 0;
}
*/
int loadElements(struct graphObj *obj, char *filename) {
	char buf[64];
	int fIndex = 0;
	FILE* f = fopen(filename, "r");
	if (f == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", filename, strerror(errno));
		return -1;
	}

	while (fscanf(f, "%[^\n]\n", buf) != EOF) {
		if (buf[0] == 'f') {
			sscanf(buf, "f %d %d %d", obj->elements + fIndex, obj->elements + (fIndex + 1), obj->elements + (fIndex + 2));
			for (int i = 0; i < 3; i++)
				obj->elements[fIndex + i]--;
			fIndex += 3;
		}
	}

	return 0;
}

struct graphObj *loadObject(char *filename) {
	struct graphObj *res = (struct graphObj*) malloc(sizeof(struct graphObj));

	res->v_size = countLines('v', filename) * 6;
	res->e_size = countLines('f', filename) * 3;
//	res->n_size = countLines("vn", filename) * 3;

	res->vertices = (float*) malloc(res->v_size * sizeof(float));
	res->elements = (GLuint*) malloc(res->e_size * sizeof(GLuint));
//	res->normals = (float*) malloc(res->n_size * sizeof(float));

	if (loadVertices(res, filename) == -1) {
		fprintf(stderr, "Cannot load object vertices from %s: %s\n", filename, strerror(errno));
		exit(-1);
	}

	if (loadElements(res, filename) == -1) {
		fprintf(stderr, "Cannot load object elements from %s: %s\n", filename, strerror(errno));
		exit(-1);
	}

	glGenBuffers(1, &(res->vbo));
	glBindBuffer(GL_ARRAY_BUFFER, res->vbo);
	glBufferData(GL_ARRAY_BUFFER, res->v_size * sizeof(float), res->vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &(res->ebo));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, res->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, res->e_size * sizeof(GLuint), res->elements, GL_STATIC_DRAW);

	res->posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(res->posAttrib);
	glVertexAttribPointer(res->posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);

	res->colAttrib = glGetAttribLocation(shaderProgram, "color");
	glEnableVertexAttribArray(res->colAttrib);
	glVertexAttribPointer(res->colAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*) (3 * sizeof(float)));

	return res;
}

void drawObject(struct graphObj *obj) {
	glDrawElements(GL_TRIANGLES, obj->e_size, GL_UNSIGNED_INT, 0);
}

void renderScene() {
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::rotate(model, elapsed_time * glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(uniModel, 1, GL_FALSE, glm::value_ptr(model));
	glDrawElements(GL_TRIANGLES, car->e_size, GL_UNSIGNED_INT, 0);
}

void display() {
	glClearColor(0, 0, 0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	renderScene();
	glutSwapBuffers();
}

void reshape(int width, int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-0.6, 0.6, -0.6, 0.6, 1.5, 250.0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, (GLsizei) width, (GLsizei) height);
}

void animate(int value) {
	elapsed_time += 0.01f;
	glutPostRedisplay();
	glutTimerFunc(40, animate, 0);
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(900, 900);
	glutCreateWindow("3.. Laboratorijska vježba");

	GLenum err = glewInit();
	if (err != GLEW_OK) {
		fprintf(stderr, "glew initialisation error: %s\n", glewGetErrorString(err));
		return -1;
	}

	shaderProgram = compileShaders(VERT_SHAD, FRAG_SHAD);
	glUseProgram(shaderProgram);
	car = loadObject(OBJ_FILE);
	glEnable(GL_DEPTH_TEST);
/*
	GLfloat vertices[] = {
		-0.5, 0.5, 0.0, 0.0, 1.0, 1.0,
		0.5, 0.5, 0.0, 0.0, 1.0, 1.0,
		0.5, -0.5, 0.0, 0.0, 1.0, 1.0,
		-0.5, -0.5, 0.0, 0.0, 1.0, 1.0
	};

	printf("%d\n", sizeof(vertices));
	exit(0);
/*
	GLuint elements[] = {
		0, 1, 2,
		2, 3, 0
	};

	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	GLuint ebo;
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

	shaderProgram = compileShaders(VERT_SHAD, FRAG_SHAD);
	glUseProgram(shaderProgram);

	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);

	GLint colAttrib = glGetAttribLocation(shaderProgram, "color");
	glEnableVertexAttribArray(colAttrib);
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*) (3 * sizeof(float)));
/*
	glGenVertexArrays(1, &(res->vao));
	glBindVertexArray(res->vao);
*/

	uniModel = glGetUniformLocation(shaderProgram, "model");

	glm::vec3 eye = glm::vec3(0.0f, 1.0f, 2.0f);
	GLint uniEye = glGetUniformLocation(shaderProgram, "eye");
	glUniform3fv(uniEye, 1, glm::value_ptr(eye));

	glm::mat4 view = glm::lookAt(
		eye,
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f)
	);
	GLint uniView = glGetUniformLocation(shaderProgram, "view");
	glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));

	glm::mat4 proj = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 1.0f, 10.0f);
	GLint uniProj = glGetUniformLocation(shaderProgram, "proj");
	glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(proj));

	glm::vec3 lightPos = glm::vec3(10.0f, 5.0f, 0.0f);
	GLint uniLightPos = glGetUniformLocation(shaderProgram, "light_pos");
	glUniform3fv(uniLightPos, 1, glm::value_ptr(lightPos));

	glm::vec3 lightCol = glm::vec3(1.0f, 1.0f, 1.0f);
	GLint uniLightCol = glGetUniformLocation(shaderProgram, "light_col");
	glUniform3fv(uniLightCol, 1, glm::value_ptr(lightCol));

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(40, animate, 0);
	glutMainLoop();

	return 0;
}

