#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

struct matrix {
    int x, y;
    double* m;
};

void m_print(struct matrix* m){
    for (int i = 0; i < m->x; i++){
        for (int j = 0; j < m->y; j++){
            printf(" %6.2f", m->m[i * (m->y) + j]);
        }
        printf("\n");
    }
    //printf("\n");
}

struct matrix *free_matrix(struct matrix *m) {
	//printf("Freeing matrix ");
	//m_print(m);
	if (m != NULL) {
		//printf("from %p to %p\n\tFreeing double array ", m, m + 1);
		if (m->m != NULL) {
			//printf("from %p to %p\n", m->m, m->m + m->x * m->y);
			free(m->m);
		} else {
			//printf("already null\n");
		}
		free(m);
	} else {
		//printf("already null\n");
	}
	
	return NULL;
}

struct matrix* m_new(int x, int y){
	//printf("Allocating new matrix..... ");
    struct matrix* m = (struct matrix*) malloc(sizeof(struct matrix));
    if (m == NULL) {
    	printf("Failed! Terminating\n");
    	exit(1);
    } else {
    	//printf("Successful! Matrix spanning from %p to %p\n", m, m + 1);
    }
    m->x = x;
    m->y = y;
    //printf("Allocating new double[]... ");
    m->m = (double*) malloc(x * y * sizeof(double));
    memset(m->m, 0, x * y * sizeof(double));
    if (m->m == NULL) {
    	printf("Failed! Terminating\n");
    	exit(1);
    } else {
    	//printf("Successful! double[] spanning from %p to %p\n", m->m, m->m + x * y);
    }
    
    return m;
}

struct matrix* m_cons_mult(double c, struct matrix* m){
    struct matrix* res = m_new(m->x, m->y);

    for (int i = 0; i < res->x * res->y; i++){
        res->m[i] = m->m[i] * c;
    }

    return res;
}

struct matrix* m_add(struct matrix* first, struct matrix* second){
    if (first->x != second->x || first->y != second->y){
        return NULL;
    }

    struct matrix* sum = m_new(first->x, first->y);

    for (int i = 0; i < sum->x * sum->y; i++){
        sum->m[i] = first->m[i] + second->m[i];
    }

    return sum;
}

struct matrix* m_red(struct matrix* first, struct matrix* second){
    struct matrix *res, *n_sec = m_cons_mult(-1, second);
    res = m_add(first, n_sec);
    free_matrix(n_sec);
    return res;
}

struct matrix* m_trans(struct matrix* m){
    struct matrix *res = m_new(m->y, m->x);

    for (int i = 0; i < m->x; i++){
        for (int j = 0; j < m->y; j++){
            res->m[i + res->y * j] = m->m[i * m->y + j];
        }
    }

    return res;
}

double m_scalar_mult(struct matrix* first, struct matrix* second){
    if (first->x * first->y != second->x * second->y){
        return 0;
    }

    double res;
    for (int i = 0; i < first->x * first->y; i++){
        res += first->m[i] * second->m[i];
    }

    return res;
}

struct matrix* m_vector_mult(struct matrix* first, struct matrix* second){
    if (first->x * first ->y != 3 || second->x * second->y != 3) {
        return NULL;
    }

    struct matrix* res = m_new(3, 1);

    res->m[0] = first->m[1] * second->m[2] - second->m[1] * first->m[2];
    res->m[1] = second->m[0] * first->m[2] - first->m[0] * second->m[2];
    res->m[2] = first->m[0] * second->m[1] - second->m[0] * first->m[1];
    return res;
}

double m_vector_norm(struct matrix* m){
    double res;
    for (int i = 0; i < m->x * m->y; i++){
        res += m->m[i] * m->m[i];
    }
    res = sqrt(res);
    return res;
}

double m_det(struct matrix* m){
    if (m->x != m->y){
        return 0;
    }

    double det = 0;
    for (int i = 0; i < m->x; i++){
        double pos = 1, neg = 1;
        for (int j = 0; j < m->y; j++){
            pos *= m->m[j * m->y + (i + j) % m->y];
            neg *= m->m[j * m->y + (i + m->y - j) % m->y];
        }
        det += pos - neg;
    }

    return det;
}

struct matrix* m_inv(struct matrix* m){
    struct matrix *res = m_new(3, 3), *tmp = m_trans(m);
    double det = m_det(m);

    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            res->m[i * 3 + j] = tmp->m[((i + 1)%3) * 3 + (j + 1)%3] * tmp->m[((i + 2)%3) * 3 + (j + 2)%3]
                    - tmp->m[((i + 2)%3) * 3 + (j + 1)%3] * tmp->m[((i + 1)%3) * 3 + (j + 2)%3];
            res->m[i * 3 + j] /= det;
        }
    }
    
    free_matrix(tmp);

    return res;
}

struct matrix* m_mult(struct matrix* first, struct matrix* second){
	//printf("Multiplying %dx%d with %dx%d matrix\n", first->x, first->y, second->x, second->y);
    if (first->y != second->x){
    	printf("Bad matrices\n");
        return NULL;
    }

    struct matrix* res = m_new(first->x, second->y);

    for (int i = 0; i < first->x; i++) {
        for (int j = 0; j < second->y; j++) {
            res->m[i * res->y + j] = 0;
            for (int k = 0; k < first->y; k++) {
                res->m[i * res->y + j] += first->m[i * first->y + k] * second->m[k * second->y + j];
            }
        }
    }

    return res;
}

